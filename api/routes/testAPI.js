var express = require('express');  // this is a must
var router = express.Router(); // this is a must
var cors = require("cors");

var corsOptions = {
    origin: 'http://localhost:3005',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}


router.get('/', cors(corsOptions), function (req, res, next) {
    res.json({ hello: 'world' });
});

module.exports = router;
